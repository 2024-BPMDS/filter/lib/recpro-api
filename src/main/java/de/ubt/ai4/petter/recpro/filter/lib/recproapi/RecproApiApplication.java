package de.ubt.ai4.petter.recpro.filter.lib.recproapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecproApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RecproApiApplication.class, args);
	}

}
