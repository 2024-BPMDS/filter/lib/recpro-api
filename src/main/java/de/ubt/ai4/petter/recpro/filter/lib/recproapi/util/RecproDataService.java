package de.ubt.ai4.petter.recpro.filter.lib.recproapi.util;

import de.ubt.ai4.petter.recpro.lib.attribute.modeling.model.RecproAttribute;
import de.ubt.ai4.petter.recpro.lib.attributepersistence.model.RecproAttributeInstance;
import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.Activity;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.Filter;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.FilterInstance;
import de.ubt.ai4.petter.recpro.lib.rating.ratingpersistence.model.RatingInstance;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
public class RecproDataService {
    @Value("${recpro.baseurl.api}")
    String baseUrl;
    public Filter getFilter(String filterId) {
        RestTemplate template = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "/api/modeling/filter/getById/" + filterId);
        return Objects.requireNonNull(template.getForObject(builder.toUriString(), Filter.class));
    }

    public FilterInstance getFilterInstance(Long tasklistId) {
        RestTemplate template = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "/api/recpro/filter/execution/getByTasklistId/" + tasklistId);
        return Objects.requireNonNull(template.getForObject(builder.toUriString(), FilterInstance.class));
    }

    public Tasklist getTasklistById(Long tasklistId) {
        RestTemplate template = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "/api/recpro/bpm/execution/tasklist/getById/" + tasklistId);
        return Objects.requireNonNull(template.getForObject(builder.toUriString(), Tasklist.class));
    }

    public List<RecproAttributeInstance> getAllAttributeInstances() {
        RestTemplate template = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "/api/recpro/attribute/execution/getAll");
        return Arrays.asList(Objects.requireNonNull(template.getForObject(builder.toUriString(), RecproAttributeInstance[].class)));
    }

    public List<RecproAttribute> getAttributes() {
        RestTemplate template = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "/api/modeling/attribute/getAll");
        return Arrays.asList(Objects.requireNonNull(template.getForObject(builder.toUriString(), RecproAttribute[].class)));
    }

    public List<RecproAttributeInstance> getAttributesByProcessInstanceIds(List<String> processInstanceIds) {
        RestTemplate template = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "/api/recpro/attribute/execution/getByProcessInstanceIds").queryParam("processInstanceIds", processInstanceIds);
        return Arrays.asList(Objects.requireNonNull(template.getForObject(builder.toUriString(), RecproAttributeInstance[].class)));
    }

    public List<RatingInstance> getRatingInstances() {
        RestTemplate template = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "/api/recpro/rating/persistence/getAll");
        return Arrays.asList(Objects.requireNonNull(template.getForObject(builder.toUriString(), RatingInstance[].class)));
    }

    public List<RatingInstance> getByUserIds(List<String> userIds) {
        RestTemplate template = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "/api/recpro/rating/persistence/getByUserIds").queryParam("userIds", userIds);
        return Arrays.asList(Objects.requireNonNull(template.getForObject(builder.toUriString(), RatingInstance[].class)));
    }

    public List<Activity> getActivities() {
        RestTemplate template = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "/api/modeling/bpm/activity");
        return Arrays.asList(Objects.requireNonNull(template.getForObject(builder.toUriString(), Activity[].class)));
    }
}
